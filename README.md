# Manager plugin for Craft CMS 3.x

Batch of utility commands for site maintenance.

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer config repositories.flowauth vcs https://bitbucket.org/flowsa/manager.git
        composer require flowsa/manager

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Manager.

## Manager Overview

A set of CLI commands used on Flow projects. Usually these CLI commands will extend other plugins that we use.

## Configuring Manager

No configuration required

## Using Manager

  ./craft manager

## Manager Roadmap

* Release it

Brought to you by [Flow communications](https://www.flowsa.com)

<?php

namespace flowsa\manager\services;

use yii\base\Component;
use flowsa\manager\Manager;
use Craft;


class ImportFromWordpressDb extends Component
{

    public function import($feedId, $offset = 0, $id = 0)
    {

        $whereId = $id ? "AND p.id = $id" : "";

        while ($offset < 100000) {

            echo "\n\n\n NEXT 10; offset is now $offset \n\n\n";

            $query = 'SELECT 
              p.id, 
              p.post_date, 
              p.post_excerpt,
              p.post_content, 
              p.post_title, 
              p.post_name, 
              pm.meta_key, 
              pm.meta_value, 
              u.user_login, 
              fimage.guid, 
              fimage.post_title as image_title, 
              fimage.post_content as image_content
              FROM wp_posts p
              LEFT JOIN wp_postmeta pm ON p.ID = pm.`post_id` 
              LEFT JOIN wp_users u ON p.post_author = u.id
              LEFT JOIN wp_posts fimage ON pm.meta_value = fimage.ID
              WHERE pm.meta_key = "_thumbnail_id"
              AND p.post_status = "publish"
              AND p.post_type = "post"
              ' . $whereId . '
              ORDER BY id DESC
              LIMIT 10
              OFFSET ' . $offset;

            // echo $query;exit;

            $offset = $offset + 10;

            $list = Craft::$app->db->createCommand($query)->queryAll();

            foreach ($list as $item) {

                $categoryQuery = '
                  SELECT t.name, t.slug
                  FROM wp_posts p 
                  LEFT JOIN wp_term_relationships tr ON tr.object_id = p.id
                  LEFT JOIN wp_term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
                  LEFT JOIN wp_terms t ON t.term_id = tt.term_id 
                  WHERE p.id = ' . $item["id"];

                $categories = Craft::$app->db->createCommand($categoryQuery)->queryAll();

                // $finalCategories = $this->mapCategories($categories);

                $item["categories"] = $categories;

                $item["post_content"] = $this->processContent($item["post_content"]);

                $item = ["root" => ["item" => $item]];

                $item = json_encode($item);

                if (file_put_contents("item.json", $item) !== false) {
                    $pwd = getcwd();

                    Manager::getInstance()->importFeed->import($feedId);
                    echo "DONE \n";

                }

                if ($id) {
                    echo "Only one entry, exiting.";
                    exit;
                }

            }

        }

    }

    public function processContent($content)
    {

        // preg split content into identifiable chunks
        $items = $this->breakContentUpAndTypeItems($content);

        $finalContent = [];

        foreach ($items as $k => $item) {

            if (isset($item["text"])) {
                $item["text"] = $this->convertLineBreaks($item["text"]);
            }

            // normalise image arrays
            if (isset($item["captionWithImage"])) {
                echo "Processing captionWithImage";
                $item["image"] = $this->processEmbeddedImages($item["captionWithImage"]);
            }

            if (isset($item["imageHtml"])) {
                $item["image"] = $this->processImageHtml($item["imageHtml"]);
            }


            $finalContent[$k] = $item;

        }

        // var_dump($finalContent); exit;
        return $finalContent;

    }

    function convertLineBreaks($content)
    {

        $content = '<p>' . implode('</p><p>', array_filter(explode("\n", $content))) . '</p>';

        return $content;
    }

    function convert_gallery($content)
    {

        // Retrieve gallery items

        var_dump($content);

        if (isset($content["text"])) {
            preg_match('/\[gallery.*ids="(.*)"\]/i', $content["text"], $matches);
        }

        $html = "";

        if (count($matches)) {

            $ids = [];
            $assets = [];
            if (isset($matches[1])) {
                $ids = explode(",", $matches[1]);
            }

            // var_dump($ids); exit;

            $html = "<div class=\"legacy-gallery\">";

            foreach ($ids as $id) {
                $criteria = craft()->elements->getCriteria(ElementType::Asset);
                $criteria->wordpressEntryId = $id;
                $assets = $criteria->find();


                foreach ($assets as $asset) {
                    var_dump($asset->url);
                    exit;

                    $html .= "<div class=\"gallery-item\">";
                    $html .= "<img src='" . $asset->url . "'/>";
                    $html .= "</div>";
                }
            }

            $html .= "</div>";

        }

        return preg_replace(
            '/\[gallery.*ids="(.*)"\]/i',
            $html,
            $content);
    }



    public function breakContentUpAndTypeItems($content)
    {


        $imagePattern = '(<img.*>)';

        $captionPattern = "(\[caption.*\[\/caption\])";

        $searchText = "/$captionPattern|$imagePattern/";

        // First split caption shortcodes
        $items = preg_split($searchText, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

        if (count($items) > 1) {
            // var_dump($items);exit;
        }

        $finalArray = [];

        foreach ($items as $k => $item) {

            if (preg_match("/" . $captionPattern . "/", $item)) {
                $type = "captionWithImage";
            } elseif (preg_match("/" . $imagePattern . "/", $item)) {
                $type = "imageHtml";
            } else {
                $type = "text";
            }

            $finalArray[$k][$type] = $item;
        }

        return $finalArray;

    }


    public function processEmbeddedImages($content)
    {

        $pattern = '/\[caption.*<img[^>]+src="([^">]+)".*\/>(.*)\[\/caption]/';

        preg_match($pattern, $content, $matches);

        $imageArray = [];

        if (is_array($matches) && (count($matches) > 0)) {

            $imageArray['imgsrc'] = isset($matches[1]) ? $matches[1] : "";
            $imageArray['imgcaption'] = isset($matches[2]) ? $matches[2] : "";

        }

        return $imageArray;

    }

    public function processImageHtml($content)
    {

        $pattern = '/<img[^>]+src="([^">]+)".*>/';

        preg_match($pattern, $content, $matches);

        $imageArray = [];

        if (is_array($matches) && (count($matches) > 0)) {

            $imageArray['imgsrc'] = isset($matches[1]) ? $matches[1] : "";

        }

        return $imageArray;

    }
}
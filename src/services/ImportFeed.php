<?php

namespace flowsa\manager\services;

use yii\base\Component;
use Craft;
use verbb\feedme\FeedMe;

use rias\notifications\Notifications;
use app\notifications\SubmissionPostAdded;

class ImportFeed extends Component
{

    /**
     * Handle manager/feed-me/do-something console commands
     *
     * The first line of this method docblock is displayed as the description
     * of the Console Command in ./craft help
     *
     * @return mixed
     */
    public function import($id, $limit = 0, $offset = 0, $debug = false)
    {

        try {
            $feed = FeedMe::$plugin->feeds->getFeedById($id);
            if ($debug) {
                $feed->debug = true;
            }

            $feedData = $feed->getFeedData();

            if ($offset) {
                $feedData = array_slice($feedData, $offset);
            }

            if ($limit) {
                $feedData = array_slice($feedData, 0, $limit);
            }

            // Do we even have any data to process?
            if (!$feedData) {
                FeedMe::info($feed, 'No feed items to process.');
                return;
            }

            $totalSteps = count($feedData);

            $feedSettings = FeedMe::$plugin->process->beforeProcessFeed($feed, $feedData);

            foreach ($feedData as $key => $data) {
                try {
                    $element = FeedMe::$plugin->process->processFeed($key, $feedSettings, $processedElementIds);
                } catch (\Throwable $e) {
                    // We want to catch any issues in each iteration of the loop (and log them), but this allows the
                    // rest of the feed to continue processing.
                    FeedMe::error($feed, $e->getMessage() . ' - ' . basename($e->getFile()) . ':' . $e->getLine());
                }


                echo('Feed item ' . ++$key . '/' . $totalSteps . ' processed' . "\n");
            }

            FeedMe::$plugin->process->afterProcessFeed($feedSettings, $feed, $processedElementIds);
        } catch (\Throwable $e) {
            // Even though we catch errors on each step of the loop, make sure to catch errors that can be anywhere
            // else in this function, just to be super-safe and not cause the queue job to die.
            FeedMe::error($feed, $e->getMessage() . ' - ' . basename($e->getFile()) . ':' . $e->getLine());
        }
    }


}
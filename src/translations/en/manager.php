<?php
/**
 * Manager plugin for Craft CMS 3.x
 *
 * Batch of utility commands for site maintenance.

 *
 * @link      https://www.flowsa.net
 * @copyright Copyright (c) 2018 Flow communications
 */

/**
 * Manager en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('manager', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow communications
 * @package   Manager
 * @since     1.0.0
 */
return [
    'Manager plugin loaded' => 'Manager plugin loaded',
];

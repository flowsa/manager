<?php
/**
 * Manager plugin for Craft CMS 3.x
 *
 * Batch of utility commands for site maintenance.
 *
 * @link      https://www.flowsa.net
 * @copyright Copyright (c) 2018 Flow communications
 */

namespace flowsa\manager\console\controllers;

use flowsa\manager\Manager;
use Craft;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * FeedMe Command
 *
 * The first line of this class docblock is displayed as the description
 * of the Console Command in ./craft help
 *
 * Craft can be invoked via commandline console by using the `./craft` command
 * from the project root.
 *
 * Console Commands are just controllers that are invoked to handle console
 * actions. The segment routing is plugin-name/controller-name/action-name
 *
 * The actionIndex() method is what is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft manager/feed-me
 *
 * Actions must be in 'kebab-case' so actionDoSomething() maps to 'do-something',
 * and would be invoked via:
 *
 * ./craft manager/feed-me/do-something
 *
 * @author    Flow communications
 * @package   Manager
 * @since     1.0.0
 */
class FeedMeController extends Controller
{


    // Public Methods
    // =========================================================================

    /**
     * Handle manager/feed-me console commands
     *
     * The first line of this method docblock is displayed as the description
     * of the Console Command in ./craft help
     *
     * @return mixed
     */
    public function actionIndex()
    {

        foreach (FeedMe::$plugin->feeds->getFeeds() as $key => $feed) {
            echo $feed->id . ": " . $feed->name . ": " . $feed->feedUrl . "\n";
        }

        return false;
    }

    /**
     * Handle manager/feed-me/do-something console commands
     *
     * The first line of this method docblock is displayed as the description
     * of the Console Command in ./craft help
     *
     * @return mixed
     */
    public function actionImport($id, $limit = 0, $offset = 0, $debug = false)
    {

        Manager::getInstance()->importFeed->import($id, $limit, $offset, $debug);

    }

    public function actionImportFromWordpressDb($feedId, $offset = 0, $id = 0)
    {

        Manager::getInstance()->importFromWordpressDb->import($feedId, $offset, $id);

    }


}

